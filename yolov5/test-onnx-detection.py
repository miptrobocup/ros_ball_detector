#!/usr/bin/env python
# coding: utf-8

import onnx
import os
import numpy as np
import onnxruntime
import cv2
import random
import time

from demo_onnx1 import detect_onnx
#from demo_onnx_old import detect_onnx
#weights = "run1/exp5/old_model/best.onnx"

weights = "run1/exp5/weights/best.onnx"

def predict(path, weights, print_=True, save_=True, conf_thres=0.3):
    t = time.time()
    img = cv2.imread(path)
    W, H = 320, 320
    img = cv2.resize(img, (W, H), interpolation=cv2.INTER_AREA)
    test_dir = 'test_' + path.split('/')[-1]
    cv2.imwrite(test_dir, img)
    rects = detect_onnx(True, test_dir, weights, conf_thres=conf_thres)
    if print_:
        print(rects)
        print(time.time() - t)
    for rect in rects[0]:
        if rect is not None and save_:
            x_min, y1, x_max, y2, conf, class_ = rect
            if class_ <3:
                print(rect)
                #need  a 232 b 263
                # we a 212 b 237
                #class2 GET 38 to 135 NEED -... TO 120
                #clsee0 GET 138 TO 156 NEED 133 TO 153
                #h_y = (y2 - y1)
                y_min = y2# +0.5*(y2-y1)
                y_max = y1
                assert y1<y2
                color = [0, 0, 0]
                color[int(class_)] = 255
                print((x_min,y_min))
                print((x_max,y_max))
                cv2.rectangle(img, (int(x_min), int(y_min)), (int(x_max), int(y_max)), tuple(color), 2)
    if save_:
        test_dir = 'test_' + path.split('/')[-1]
        cv2.imwrite(test_dir, img)




paths = ['data/custom/images/valid/scene4_frame0248.jpg',
         'data/custom/images/valid/scene4_frame0284.jpg',
         'data/custom/images/valid/scene4_frame0278.jpg',
         'data/custom/images/valid/scene4_frame0256.jpg',
         'data/custom/images/valid/scene4_frame0294.jpg',
         'data/custom/images/valid/scene4_frame0243.jpg',
         'data/custom/images/valid/scene4_frame0247.jpg',
         'data/custom/images/valid/scene4_frame0249.jpg',
         'data/custom/images/valid/scene4_frame0228.jpg',
         'data/custom/images/valid/scene4_frame0233.jpg',
         'data/custom/images/valid/scene4_frame0236.jpg',
         'data/custom/images/valid/scene4_frame0239.jpg',
         'data/custom/images/valid/scene4_frame0307.jpg',
         'data/custom/images/valid/scene4_frame0302.jpg',
         'data/custom/images/valid/scene4_frame0299.jpg',
         'data/custom/images/valid/scene1_frame0038.jpg',
         'data/custom/images/valid/scene1_frame0114.jpg',
         'data/custom/images/valid/scene1_frame0109.jpg',
         'data/custom/images/valid/scene1_frame0093.jpg',
         'data/custom/images/valid/scene1_frame0080.jpg',
         'data/custom/images/valid/scene1_frame0036.jpg',
         'data/custom/images/valid/scene1_frame0032.jpg',
         'data/custom/images/valid/scene1_frame0170.jpg',
         'data/custom/images/valid/scene1_frame0162.jpg',
         'data/custom/images/valid/scene1_frame0179.jpg',
         'data/custom/images/valid/scene1_frame0189.jpg',
         'data/custom/images/valid/scene2_frame0028.jpg',
         'data/custom/images/valid/scene2_frame0020.jpg',
         'data/custom/images/valid/scene2_frame0010.jpg']
for path in paths:
    predict(path, weights)


