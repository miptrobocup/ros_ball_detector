import numpy as np
import onnxruntime
import sys


def w_bbox_iou(box1, box2, x1y1x2y2=True): # done
    if not x1y1x2y2:
        b1_x1, b1_x2 = box1[:, 0] - box1[:, 2] / 2, box1[:, 0] + box1[:, 2] / 2
        b1_y1, b1_y2 = box1[:, 1] - box1[:, 3] / 2, box1[:, 1] + box1[:, 3] / 2
        b2_x1, b2_x2 = box2[:, 0] - box2[:, 2] / 2, box2[:, 0] + box2[:, 2] / 2
        b2_y1, b2_y2 = box2[:, 1] - box2[:, 3] / 2, box2[:, 1] + box2[:, 3] / 2
    else:
        b1_x1, b1_y1, b1_x2, b1_y2 = box1[:, 0], box1[:, 1], box1[:, 2], box1[:, 3]
        b2_x1, b2_y1, b2_x2, b2_y2 = box2[:, 0], box2[:, 1], box2[:, 2], box2[:, 3]

    inter_rect_x1 = np.maximum(b1_x1, b2_x1)
    inter_rect_y1 = np.maximum(b1_y1, b2_y1)
    inter_rect_x2 = np.minimum(b1_x2, b2_x2)
    inter_rect_y2 = np.minimum(b1_y2, b2_y2)

    inter_area = np.clip(inter_rect_x2 - inter_rect_x1 + 1, 0, None) * \
                np.clip(inter_rect_y2 - inter_rect_y1 + 1, 0, None)

    b1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
    b2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)

    iou = inter_area / (b1_area + b2_area - inter_area + 1e-16)

    return iou

def filter(image_pred, label, conf_thres=0.4, gh=5, gw=5, lh=500, lw=500):  # TODO(roman): change to (240, 320) when new network is available
    if image_pred.shape[1] < 4:
        return np.array([])
    label_mask = np.isclose(image_pred[:, -1], label).squeeze()
    conf_mask = ((image_pred[:, 4] >= conf_thres)).squeeze()
    less_mask = ((image_pred[:, 3] < 480) & (image_pred[:, 2] < 640) & (image_pred[:, 2] - image_pred[:, 0] < lh) & (image_pred[:, 3] - image_pred[:, 1] < lw)).squeeze()  # TODO(roman): change to (240, 320) when new network is available
    great_mask = ((image_pred[:, 2] - image_pred[:, 0] > gh) & (image_pred[:, 3] - image_pred[:, 1] > gw)).squeeze()
    mask = (~label_mask) | (conf_mask & less_mask & great_mask)
    
    image_pred = image_pred[mask]
    return image_pred

def w_non_max_suppression(prediction, num_classes, conf_thres=0.5, nms_thres=0.4):
    box_corner = np.array(prediction)
    box_corner[:, :, 0] = prediction[:, :, 0] - prediction[:, :, 2] / 2
    box_corner[:, :, 1] = prediction[:, :, 1] - prediction[:, :, 3] / 2
    box_corner[:, :, 2] = prediction[:, :, 0] + prediction[:, :, 2] / 2
    box_corner[:, :, 3] = prediction[:, :, 1] + prediction[:, :, 3] / 2
    prediction[:, :, :4] = box_corner[:, :, :4]

    output = [None for _ in range(len(prediction))]
    for image_i, image_pred in enumerate(prediction):
        image_pred = image_pred[(image_pred[:, 4] > conf_thres).squeeze()]
        
        if not image_pred.shape[0]:
            continue
        class_conf = image_pred[:, 5:5 + num_classes].max(axis=1, keepdims=True)
        class_pred = np.expand_dims(np.argmax(image_pred[:, 5:5 + num_classes], axis=1), axis=1)
        # (x1, y1, x2, y2, obj_conf, class_conf, class_pred)
        detections = np.concatenate((image_pred[:, :5], class_conf.astype(float), class_pred.astype(float)), 1)
        detections = filter(detections, 0, gh=20, gw=20, lh=300, lw=300, conf_thres=0.2)  # TODO(roman): change to (240, 320) when new network is available
        detections = filter(detections, 1, gh=50, gw=25, lh=500, lw=500, conf_thres=0.38)
        detections = filter(detections, 2, gh=10, gw=10, lh=500, lw=500, conf_thres=0.34)
        if detections.shape[0] == 0:
            continue

        unique_labels = np.unique(detections[:, -1])

        for c in unique_labels:
            detections_class = detections[detections[:, -1] == c]
            conf_sort_index = np.argsort(-detections_class[:, 4])  # descending
            detections_class = detections_class[conf_sort_index]
            max_detections = []
            while detections_class.shape[0]:
                max_detections.append(np.expand_dims(detections_class[0], axis=0))
                if len(detections_class) == 1:
                    break
                ious = w_bbox_iou(max_detections[-1], detections_class[1:])
                detections_class = detections_class[1:][ious < nms_thres if not np.isclose(c, 2) else ious < 1e-5]
            max_detections = np.concatenate(max_detections)
            # Add max detections to outputs
            output[image_i] = max_detections if output[image_i] is None else np.concatenate((output[image_i], max_detections))

    return output


# class names
class_names = ['ball', 'robot', 'goal']


def letterbox_image(image, size):
    image = np.resize(image, (640, 640, 3))  # TODO(roman): change to (240, 320) when new network is available
    return image


def clip_coords(boxes, img_shape):
    # Clip bounding xyxy bounding boxes to image shape (height, width)
    boxes[:, 0] = np.clip(boxes[:, 0], 0, img_shape[1])  # x1
    boxes[:, 1] = np.clip(boxes[:, 1], 0, img_shape[0])  # y1
    boxes[:, 2] = np.clip(boxes[:, 2], 0, img_shape[1])  # x2
    boxes[:, 3] = np.clip(boxes[:, 3], 0, img_shape[0])  # y2


def scale_coords(img1_shape, coords, img0_shape, ratio_pad=None):
    # Rescale coords (xyxy) from img1_shape to img0_shape
    if ratio_pad is None:  # calculate from img0_shape
        gain = max(img1_shape) / max(img0_shape)  # gain  = old / new
        pad = (img1_shape[1] - img0_shape[1] * gain) / 2, (img1_shape[0] - img0_shape[0] * gain) / 2  # wh padding
    else:
        gain = ratio_pad[0][0]
        pad = ratio_pad[1]

    coords[:, [0, 2]] -= pad[0]  # x padding
    coords[:, [1, 3]] -= pad[1]  # y padding
    coords[:, :4] /= gain
    clip_coords(coords, img0_shape)
    return coords


def xyxy2xywh(x):
    # Convert nx4 boxes from [x1, y1, x2, y2] to [x, y, w, h] where xy1=top-left, xy2=bottom-right
    y = np.zeros_like(x)
    y[:, 0] = (x[:, 0] + x[:, 2]) / 2  # x center
    y[:, 1] = (x[:, 1] + x[:, 3]) / 2  # y center
    y[:, 2] = x[:, 2] - x[:, 0]  # width
    y[:, 3] = x[:, 3] - x[:, 1]  # height
    return y


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = np.zeros_like(x)
    y[:, 0] = x[:, 0] - x[:, 2] / 2  # top left x
    y[:, 1] = x[:, 1] - x[:, 3] / 2  # top left y
    y[:, 2] = x[:, 0] + x[:, 2] / 2  # bottom right x
    y[:, 3] = x[:, 1] + x[:, 3] / 2  # bottom right y
    return y


def sigmoid(x): # done
    return 1 / (1 + np.exp(-x))


def detect_onnx(image_src, session, confidence=0.52): # done
    assert session is not None, "please provide onnx session"
    
    num_classes = 3
    anchors = [[116, 90, 156, 198, 373, 326], [30, 61, 62, 45, 59, 119], [10, 13, 16, 30, 33, 23]]  # 5s

    # print("The model expects input shape: ", session.get_inputs()[0].shape)
    batch_size = session.get_inputs()[0].shape[0]
    img_size_h = session.get_inputs()[0].shape[2]
    img_size_w = session.get_inputs()[0].shape[3]

    # input
    resized = letterbox_image(image_src, (img_size_w, img_size_h))

    img_in = np.transpose(resized, (2, 0, 1)).astype(np.float32)  # HWC -> CHW
    img_in = np.expand_dims(img_in, axis=0)
    img_in /= 255.0
    # print("Shape of the image input shape: ", img_in.shape)

    # inference
    input_name = session.get_inputs()[0].name
    outputs = session.run(None, {input_name: img_in})
    
    batch_detections = []
    
    boxs = []
    anchor_grid = np.array(anchors, dtype=float).reshape(3, 1, -1, 1, 1, 2)

    if len(outputs) == 4:
        outputs = [outputs[1], outputs[2], outputs[3]]

    for index, out in enumerate(outputs):
        batch = out.shape[1]
        feature_w = out.shape[2]
        feature_h = out.shape[3]

        # Feature map corresponds to the original image zoom factor
        stride_w = int(img_size_w / feature_w)
        stride_h = int(img_size_h / feature_h)

        grid_x, grid_y = np.meshgrid(np.arange(feature_w), np.arange(feature_h))

        # cx, cy, w, h
        pred_boxes = np.array(out[..., :4], dtype=float)
        pred_boxes[..., 0] = (sigmoid(out[..., 0]) * 2.0 - 0.5 + grid_x) * stride_w  # cx
        pred_boxes[..., 1] = (sigmoid(out[..., 1]) * 2.0 - 0.5 + grid_y) * stride_h  # cy
        pred_boxes[..., 2:4] = (sigmoid(out[..., 2:4]) * 2) ** 2 * anchor_grid[index]  # wh

        conf = sigmoid(out[..., 4])
        pred_cls = sigmoid(out[..., 5:])

        output = np.concatenate(
            (
                pred_boxes.reshape(batch_size, -1, 4),
                conf.reshape(batch_size, -1, 1),
                pred_cls.reshape(batch_size, -1, num_classes)
            ),
            axis=-1
        )
        boxs.append(output)

    outputx = np.concatenate(boxs, axis=1)
    # NMS
    batch_detections = w_non_max_suppression(outputx, num_classes, conf_thres=confidence, nms_thres=0.1)

    return batch_detections


def detect_scaled(image_src, onnx_session, src_img_size=(480, 640), confidence=0.52):  # TODO(roman): change to (240, 320) when new network is available
    detections = detect_onnx(image_src, onnx_session, confidence=confidence)[0]
    # scaling
    if detections is None:
        return np.array([], dtype=np.float64)
    boxs = detections[..., :4]
    boxs[:, :] = scale_coords((480, 640), boxs[:, :], src_img_size).round()  # TODO(roman): change to (240, 320) when new network is available
    detections[..., :4] = boxs
    
    return detections

