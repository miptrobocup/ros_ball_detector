# NAO ball detector on ROS

## Contents
* [Introduction](https://gitlab.com/miptrobocup/ros_ball_detector/-/blob/master/README.md#introduction)
* [Method](https://gitlab.com/miptrobocup/ros_ball_detector/-/tree/master#method)
* [Results](https://gitlab.com/miptrobocup/ros_ball_detector/-/tree/master#results)
* [Reproduce](https://gitlab.com/miptrobocup/ros_ball_detector/-/tree/master#reproduce)

## Introduction

Robots see, analyze, and make decisions more like humans every day, thanks to advances in converging technologies like artificial intelligence, machine learning, and computer vision. Developing such visual analysis logic for NAO robots involves implementing solutions that can determine the orientation of objects, deal with moving objects, and perform navigation. All these components help to improve the game speed and entertainment. So, teams have to implement them on the robot in an efficient manner.

## Method

For object detection tasks, we utilize modern neural network architecture &mdash; YOLOv5-small. We fine-tune the pre-trained model on [SPQR Team NAO image dataset](https://www.diag.uniroma1.it/~labrococo/?q=node/459). Currently, we use it to detect the ball only. But as soon as we finish the markup of our multi-object(ball, robots, humans) dataset, we will be able to improve the quality of our detector to use it for all vision tasks. 

Our approach is plug-and-play because YOLOv5-small has enough capacity to fit even an 80-label dataset. Note that we can do this upgrade almost without any additional computation overhead.

To run our detector on NAO, we exploit _ONNX Runtime_ and `numpy`-like stack(`numpy`, `skimage`) to preprocess images. Gentoo prefix allows us to run all necessary packages, including the ROS ecosystem, on the robot.

## Results

|Learning curves|
|------|
|![Learning curves](examples/results.png "Learning curves")|


| Examples | |
| ------ | ------ |
| ![Strange background](examples/img_3299.png "Strange background") | ![Center](examples/scene1_frame0178.jpg "Center") |
| ![Several balls](examples/several_balls.png "Several balls") | ![Bottom example](examples/bottom_example.png "Bottom example") |

## Reproduce

First of all, you should prepare the dataset:
1. Download and unpack "Images with ball — annotated", "Negative samples from top camera", and "Negative samples from bottom camera" from the[SPQR Team NAO dataset](https://www.diag.uniroma1.it/~labrococo/?q=node/459).
2. Execute [prepare-dataset.py](https://gitlab.com/miptrobocup/ros_ball_detector/-/blob/master/prepare-dataset.ipynb).

Then, building docker image(probably you should somehow change `Dockerfile` for your needs):

```docker build . --tag starkit-ball-detector:1.0```

Running docker container:

```sudo docker run --rm -it --gpus all --network host --mount type=bind,source=/dev/shm,target=/dev/shm -v /home/gorb-roman/robots:/robots starkit-ball-detector:1.0```

After that, launching trainning:

```python train.py --img 640 --batch 128 --epochs 300 --data sqpr.yaml --weights yolov5s.pt```

---

**Note:**<br>
You can download our best experiment dump(checkpoint + statistics + curves) with image size 640*640 from [here](https://drive.google.com/drive/folders/1B_PvSfxoMEaSL_TogeyV6B02KOSznSwX?usp=sharing) and place it to `yolov5/runs/train/`. UPD. The dump of the best experiment with image size 320*320 is [here](https://drive.google.com/file/d/1l1IZ3vC-Y-jdSGTx_PgI_BsZswu6YNF3/view?usp=sharing)

---

Once we finished the training, we can export our model to onnx:

1. ```cd yolov5s```

2. ```python3 models/export.py --weights runs/train/best-exp/weights/best.pt --img 640 --batch 1 --simplify```
You only need to provide the path to `best.onnx` file from the output of the last command.

Congratulations, you are ready to run ROS node!!! 
