#!/usr/bin/env /tmp/gentoo/usr/bin/python3

import os
import onnxruntime
from scipy.ndimage import zoom
import matplotlib.pyplot as plt
from time import time

from sensor_msgs.msg import Image

import rospy
from std_msgs.msg import Float32MultiArray

from yolov5.onnx_detection_numpy import detect_scaled
from ros_numpy import numpify


DEBUG = os.environ['DEBUG'] == 'True' if 'DEBUG' in os.environ else False


def visualize(img, detections):
    mapping = {
        0: 'magenta',
        1: 'lime',
        2: 'blue'
    }
    plt.figure(figsize=(10, 10))
    plt.imshow(img)
    for d in detections:
        x1, y1, x2, y2 = d[:4]
        plt.plot([x1, x1, x2, x2, x1], [y1, y2, y2, y1, y1], color=mapping[d[-1]])

    plt.savefig('./img.png')


def prepare_img(message):
    img = zoom(numpify(message), (2, 2, 1))
    img = img[:480, :640, :]
    if DEBUG:
        rospy.loginfo(f"input image shape: {img.shape}")
    return img


def create_detector(onnx_session, publisher_mapping):
    def process_image(message):
        img = prepare_img(message)

        if DEBUG:
            rospy.loginfo('starting detection')
            start = time()
        detections = detect_scaled(img, onnx_session, confidence=0.175)
        if DEBUG:
            rospy.loginfo(f'finised detection, elapsed time = {time() - start}, have {detections}')
            # heavy dump on disk
            visualize(img, detections)
        
        
        if DEBUG:
            rospy.loginfo("started publishing")

        for d in detections:
            label = d[-1]
            x1, y1, x2, y2 = d[:4]
            publisher = publisher_mapping[label]
            # TODO(roman): test message creation
            msg = Float32MultiArray()
            msg.data = [x1, y1, x2, y2]
            # publishing
            publisher.publish(msg)

        if DEBUG:
            rospy.loginfo("finished publishing")
    
    return process_image


def listener():
    rospy.init_node('listener', anonymous=True)
    
    onnx_session = onnxruntime.InferenceSession("yolov5/golden-model/best.onnx")
    
    top_publishers = {
        0: rospy.Publisher('ball_on_image/top', Float32MultiArray, queue_size=10),
        1: rospy.Publisher('robot_on_image/top', Float32MultiArray, queue_size=10),
        2: rospy.Publisher('gate_on_image/top', Float32MultiArray, queue_size=10),
    }
    bottom_publishers = {
        0: rospy.Publisher('ball_on_image/bottom', Float32MultiArray, queue_size=10),
        1: rospy.Publisher('robot_on_image/bottom', Float32MultiArray, queue_size=10),
        2: rospy.Publisher('gate_on_image/bottom', Float32MultiArray, queue_size=10),
    }
    
    top_callback = create_detector(onnx_session, top_publishers)
    bottom_callback = create_detector(onnx_session, bottom_publishers)
    
    rospy.loginfo("started detection loop")
    while not rospy.is_shutdown():
        msg = rospy.wait_for_message("/naoqi_driver/camera/bottom/image_raw", Image)
        bottom_callback(msg)
        msg = rospy.wait_for_message("/naoqi_driver/camera/top/image_raw", Image)
        top_callback(msg)


if __name__ == '__main__':
    listener()
